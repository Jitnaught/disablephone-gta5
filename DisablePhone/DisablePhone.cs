﻿using System;
using GTA;
using GTA.Native;

namespace DisablePhone
{
	public class DisablePhone : Script
	{
		const string CELLPHONE_SCRIPT = "cellphone_controller";

		private bool enabled;
		private readonly int cheatString;

		public DisablePhone()
		{
			enabled = Settings.GetValue("SETTINGS", "ENABLED_ON_STARTUP", true);
			cheatString = Game.GenerateHash(Settings.GetValue("SETTINGS", "CHEAT_STRING", "togglephone"));

			Interval = 0;
			Tick += DisablePhone_Tick;
		}

		private void StartCellphoneScript()
		{
			Function.Call(Hash.REQUEST_SCRIPT, CELLPHONE_SCRIPT);

			for (int i = 0; i < 100; i++)
			{
				if (Function.Call<bool>(Hash.HAS_SCRIPT_LOADED, CELLPHONE_SCRIPT)) break;

				Wait(1);
			}

			if (Function.Call<bool>(Hash.HAS_SCRIPT_LOADED, CELLPHONE_SCRIPT))
			{
				Function.Call(Hash.START_NEW_SCRIPT, CELLPHONE_SCRIPT, 1424);
				Function.Call(Hash.SET_SCRIPT_AS_NO_LONGER_NEEDED, CELLPHONE_SCRIPT);
			}
		}

		private void DisablePhone_Tick(object sender, EventArgs e)
		{
			if (Function.Call<bool>(Hash._0x557E43C447E700A8, cheatString)) //_HAS_CHEAT_STRING_JUST_BEEN_ENTERED
			{
				enabled = !enabled;

				if (!enabled)
				{
					StartCellphoneScript();
				}

				UI.ShowSubtitle($"DisablePhone {(enabled ? "enabled" : "disabled")}");
			}

			if (enabled)
			{
				Function.Call(Hash.TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME, CELLPHONE_SCRIPT);
				Game.DisableControlThisFrame(0, Control.Phone);
			}
		}
	}
}
